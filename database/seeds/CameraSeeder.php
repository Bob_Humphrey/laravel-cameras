<?php

use Illuminate\Database\Seeder;
use App\Camera;

class CameraSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(Camera::class, 15)->create();
  }
}
