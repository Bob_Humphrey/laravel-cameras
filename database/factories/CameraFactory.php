<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Camera;
use Faker\Generator as Faker;

$factory->define(Camera::class, function (Faker $faker) {
  return [
    'name' => $faker->word,
    'image' => $faker->word . '.jpg',
    'description' => $faker->text(180),
    'price' => $faker->numberBetween(50, 250)
  ];
});
