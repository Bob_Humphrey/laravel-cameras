@php
$active = "text-blue-500 hover:text-blue-500 lg:px-4 py-2";
$inactive = "text-blue-800 hover:text-blue-500 lg:px-4 py-2";
@endphp

<a href="{{ url('/cameras') }}" class="{{ request()->is('cameras') ? $active : $inactive }}">
  Cameras
</a>
<a href="{{ url('/cart') }}" class="{{ request()->is('cart') ? $active : $inactive }}">
  @livewire('cart-count')
</a>