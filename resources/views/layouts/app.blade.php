<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description"
    content="A simulated online store that sells vintage cameras, built with Laravel, Livewire, Tailwind CSS, and Turbolinks.">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  @livewireScripts
  <script src="{{ asset('js/all.js') }}" defer data-turbolinks-track="true"></script>

  <!-- Fonts -->

  <!-- Styles -->
  @livewireStyles
  <link href="{{ asset('css/all.css') }}" rel="stylesheet" data-turbolinks-track="true">
</head>

<body>
  <div id="app">
    <nav class="bg-gray-50 py-2" x-data="{ showMenu: false }">
      <div class="flex flex-col lg:flex-row lg:justify-between lg:items-center w-11/12 lg:w-5/6 mx-auto py-3">

        <div class="flex justify-between items-center">

          <div class="flex lg:justify-start">
            <a href="/" class="flex flex-col lg:flex-row w-full">
              <h1 class="font-nunito_bold text-3xl text-blue-900 hover:text-blue-500">
                {{ config('app.name', 'Laravel Application') }}
              </h1>
            </a>
          </div>

          <div class="lg:hidden w-8 cursor-pointer" @click="showMenu=!showMenu" x-show="!showMenu">
            @svg('menu', 'fill-current')
          </div>

          <div class="lg:hidden w-8 cursor-pointer" @click="showMenu=!showMenu" x-show="showMenu">
            @svg('close', 'fill-current')
          </div>

        </div>

        <div
          class="hidden lg:flex flex-row justify-end text-lg font-nunito_light lg:text-xl text-blue-900 leading-none pb-8 lg:pb-0">
          @include('layouts.mainmenu')
        </div>

        <div
          class="flex flex-col lg:hidden text-lg font-nunito_light lg:text-xl text-blue-900 leading-none pb-8 lg:pb-0"
          x-show="showMenu">
          @include('layouts.mainmenu')
        </div>

      </div>
    </nav>

    <main class="py-4 w-11/12 lg:w-3/5 mx-auto">
      @yield('content')
    </main>

    @include('layouts.footer')

  </div>
</body>

</html>