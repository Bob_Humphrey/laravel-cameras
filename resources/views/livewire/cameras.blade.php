@php
use App\Facades\Cart;
@endphp
<div class="w-full sm:flex flex-wrap -mx-2">
  @foreach ($cameras as $camera)
  @php
  $showButton = Cart::inCart($camera) ? "hidden" : "";
  @endphp
  <div class="flex items-stretch sm:w-1/2 md:w-1/3 px-2 mb-4">
    <div class="flex flex-col bg-gray-100 rounded-lg overflow-hidden">
      <div class="flex-grow">
        <img class="w-full" src={{ asset('img/' . $camera->image) }} alt={{ $camera->name }} />
        <div class="px-6 py-4">
          <div class="text-lg font-nunito_bold pb-2">{{ $camera->name }} </div>
          <div class="font-nunito_extrabold text-gray-400 pb-2">
            ${{ number_format($camera->price, 2) }}
          </div>
          <div class="text-sm font-nunito_light">
            {{ $camera->description }}
          </div>
        </div>
      </div>
      <div class={{ $showButton }}>
        <div wire:click="addToCart({{ $camera->id }})"
          class="w-3/5 font-nunito_bold text-gray-500 text-center border border-gray-400 hover:bg-gray-400 hover:text-white p-1 mx-auto mb-4 rounded-lg cursor-pointer">
          Add to Cart
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>