@php
use App\Facades\Cart;
@endphp

<div class="w-full sm:flex flex-wrap -mx-2">
  @if(count($cart['cameras']) > 0)
  <table class="w-full">
    <tr class="grid grid-cols-12 text-sm font-nunito_bold border-b border-gray-300">
      <th class="col-span-6 text-left py-3">Camera</th>
      <th class="col-span-3 text-right py-3">Price</th>
      <th class="col-span-3 text-right py-3"></th>
    </tr>
    @foreach($cart['cameras'] as $camera)
    <tr class="grid grid-cols-12 text-sm font-nunito_light border-b border-gray-300">
      <td class="col-span-6 py-3">{{ $camera->name }}</td>
      <td class="col-span-3 text-right py-3">
        {{ number_format($camera->price, 2) }}
      </td>
      <td wire:click="removeFromCart({{ $camera->id }})"
        class="col-span-3 text-right hover:text-blue-500 py-3 cursor-pointer">
        Remove from Cart
      </td>
    </tr>
    @endforeach
    <tr class="grid grid-cols-12 text-sm font-nunito_bold border-b border-gray-300">
      <td class="col-span-6 text-left py-3">Total</td>
      <td class="col-span-3 text-right py-3">
        {{ number_format(Cart::total(), 2) }}
      </td>
      <td class="col-span-3 text-right py-3"></td>
    </tr>
  </table>
  <div class="flex items-center w-full pt-6">
    <button wire:click="emptyCart()"
      class="font-nunito_bold text-gray-500 text-center border border-gray-400 hover:bg-gray-400 hover:text-white py-1 px-3 mx-auto mb-4 rounded-lg cursor-pointer">
      Empty the Cart
    </button>
  </div>
  @else
  <div class="flex w-full items-center justify-center text-gray-700 bg-gray-200 h-16 my-32">
    <div class="text-xl font-nunito_bold text-center leading-tight">
      The shopping cart is empty.
    </div>
  </div>
  @endif
</div>