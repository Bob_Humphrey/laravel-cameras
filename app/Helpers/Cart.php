<?php


namespace App\Helpers;

use App\Camera;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class Cart
{
  public function __construct()
  {
    if ($this->get() === null)
      $this->set($this->empty());
  }

  public function inCart(Camera $camera): bool
  {
    $cart = $this->get();
    $cameras = $cart["cameras"];
    if (in_array($camera, $cameras)) {
      return true;
    }
    return false;
  }

  public function count(): int
  {
    $cart = $this->get();
    $cameras = $cart["cameras"];
    return count($cameras);
  }

  public function total(): float
  {
    $cart = $this->get();
    $cameras = $cart["cameras"];
    $collection = collect($cameras);
    return  $collection->sum('price');
  }

  public function add(Camera $camera): void
  {
    $cart = $this->get();
    if (!$this->inCart($camera)) {
      array_push($cart['cameras'], $camera);
    }
    $this->set($cart);
  }

  public function remove(int $cameraId): void
  {
    $cart = $this->get();
    array_splice($cart['cameras'], array_search($cameraId, array_column($cart['cameras'], 'id')), 1);
    $this->set($cart);
  }

  public function clear(): void
  {
    $this->set($this->empty());
  }

  public function empty(): array
  {
    return [
      'cameras' => [],
    ];
  }

  public function get(): ?array
  {
    return request()->session()->get('cart');
  }

  private function set($cart): void
  {
    request()->session()->put('cart', $cart);
  }
}
