<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Facades\Cart;
use App\Camera;

class Cameras extends Component
{
  public $cameras;

  public function mount(): void
  {
    $this->cameras = Camera::orderBy('id')->get();
  }

  public function render()
  {
    return view('livewire.cameras');
  }

  public function addToCart(int $cameraId): void
  {
    Cart::add(Camera::where('id', $cameraId)->first());
    $this->emit('cameraAdded');
  }
}
