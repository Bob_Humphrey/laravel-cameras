<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Facades\Cart;

class CartCount extends Component
{

  public $cartCountDisplay;

  protected $listeners = [
    'cameraAdded' => 'updateCartCount',
    'cameraRemoved' => 'updateCartCount',
    'emptyCart' => 'updateCartCount'
  ];

  public function mount(): void
  {
    $this->updateCartCount();
  }

  public function render()
  {
    return view('livewire.cart-count');
  }

  public function updateCartCount(): void
  {
    $count = Cart::count();
    // $cameras = $count === 1 ? "camera" : "cameras";
    $this->cartCountDisplay = "Shopping Cart ($count)";
  }
}
